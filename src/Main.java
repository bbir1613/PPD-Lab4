import domain.ConcurrentSortedDoublyLinkedList;
import domain.IterableConcurrentSortedDoublyLinkedList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
        ConcurrentSortedDoublyLinkedList<Integer> concurrentSortedDoublyLinkedList = new IterableConcurrentSortedDoublyLinkedList<>();
        concurrentSortedDoublyLinkedList = testInsert(concurrentSortedDoublyLinkedList);
        System.out.println(concurrentSortedDoublyLinkedList);
        System.out.println(concurrentSortedDoublyLinkedList.getSize());
        concurrentSortedDoublyLinkedList = testDelete(concurrentSortedDoublyLinkedList);
        IterableConcurrentSortedDoublyLinkedList<Integer> lst = (IterableConcurrentSortedDoublyLinkedList) concurrentSortedDoublyLinkedList;
        System.out.println(concurrentSortedDoublyLinkedList);
        for (Integer aLst : lst) {
            System.out.print(aLst);
            System.out.print(",");
        }
        System.out.println();
        System.out.println(concurrentSortedDoublyLinkedList.getSize());
        Iterator<Integer> it = lst.backwardIterator();
        while(it.hasNext()){
            System.out.print(it.next());
            System.out.print(",");
        }
        Iterator<Integer> rwIt = lst.readWriteiterator();
        rwIt.remove();
        rwIt.next();
        rwIt.next();
        rwIt.remove();
        System.out.println();
        for (Integer aLst : lst) {
            System.out.print(aLst);
            System.out.print(",");
        }
        System.out.println("Hello World!");
    }

    private static ConcurrentSortedDoublyLinkedList<Integer> testInsert(final ConcurrentSortedDoublyLinkedList<Integer> concurrentSortedDoublyLinkedList) {
        final int nrOfIterations = 100;
        List<Thread> threadList = new ArrayList<>();
        generateThreads(threadList, () -> {
            for (int j = 0; j < nrOfIterations; ++j) {
                concurrentSortedDoublyLinkedList.insert(ThreadLocalRandom.current().nextInt(0, 20));
            }
        });
        startThreads(threadList);
        joinThreads(threadList);
        return concurrentSortedDoublyLinkedList;
    }

    private static ConcurrentSortedDoublyLinkedList<Integer> testDelete(ConcurrentSortedDoublyLinkedList<Integer> concurrentSortedDoublyLinkedList) {
        final int nrOfIterations = 100;
        List<Thread> threadList = new ArrayList<>();
        generateThreads(threadList, () -> {
            for (int j = 0; j < nrOfIterations; ++j) {
                concurrentSortedDoublyLinkedList.remove(ThreadLocalRandom.current().nextInt(0, 50));
            }
        });
        startThreads(threadList);
        joinThreads(threadList);
        return concurrentSortedDoublyLinkedList;
    }

    private static List<Thread> generateThreads(List<Thread> threadList, Runnable runnable) {
        final int nrOfThreads = 10;
        for (int i = 0; i < nrOfThreads; ++i) {
            threadList.add(new Thread(runnable));
        }
        return threadList;
    }

    private static void startThreads(List<Thread> threadList) {
        threadList.forEach(Thread::start);
    }

    private static void joinThreads(List<Thread> threadList) {
        for (Thread th : threadList) {
            try {
                th.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
