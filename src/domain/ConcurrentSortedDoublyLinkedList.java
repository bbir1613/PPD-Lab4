package domain;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConcurrentSortedDoublyLinkedList<E extends Comparable<E>> {
    Node first;
    Node last;
    AtomicInteger size;

    public ConcurrentSortedDoublyLinkedList() {
        first = new Node();
        last = new Node();
        first.next = last;
        last.previous = first;
        size = new AtomicInteger();
        size.set(0);
    }

    public int getSize() {
        return size.get();
    }

    public void insert(E element) {
        first.lock.lock();
        first.next.lock.lock();
        Node start = first;
        Node next = start.next;
        while (next != null && next.element != null && element.compareTo(next.element) > 0) {
            next.next.lock.lock();
            start.lock.unlock();
            start = next;
            next = start.next;
        }
        Node insertNode = new Node(start, next, element);
        start.next = insertNode;
        next.previous = insertNode;
        start.lock.unlock();
        next.lock.unlock();
        size.addAndGet(1);
    }

    public void remove(int position) {
        if (position >= size.get()) {
            return;
        }
        Node previous = null;
        Node deletedNode = null;
        Node next = null;

        try {
            first.lock.lock();
            first.next.lock.lock();
            first.next.next.lock.lock();
            previous = first;
            deletedNode = previous.next;
            next = deletedNode.next;
            for (int i = 0; i < position; ++i) {
                next.next.lock.lock();
                previous.lock.unlock();
                previous = deletedNode;
                deletedNode = previous.next;
                next = deletedNode.next;
            }
            previous.next = next;
            next.previous = previous;
            previous.lock.unlock();
            next.lock.unlock();
            deletedNode.lock.unlock();

            size.addAndGet(-1);
        } catch (Exception ex) {
            //...
        } finally {
            if (previous != null) {
                try {
                    previous.lock.unlock();
                } catch (Exception ex) {
                    //...
                }
            }
            if (deletedNode != null) {
                try {
                    deletedNode.lock.unlock();
                } catch (Exception ex) {
                    //...
                }
            }
            if (next != null) {
                try {
                    next.lock.unlock();
                } catch (Exception ex) {
                    //..
                }
            }
        }
    }

    public E get(int position) {
        if (position >= size.get()) {
            return null;
        }
        first.lock.lock();
        first.next.lock.lock();
        Node start = first.next;
        first.lock.unlock();
        for (int i = 0; i < position; ++i) {
            start.next.lock.lock();
            start = start.next;
            start.previous.lock.unlock();
        }
        if (start != null) {
            E element = start.element;
            start.lock.unlock();
            return element;
        }
        return null;
    }

    @Override
    public String toString() {
        Node start = first.next;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < size.get(); ++i) {
            stringBuilder.append(start.element).append(',');
            start = start.next;
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        return stringBuilder.toString();
    }

    class Node {
        Node next;
        Node previous;
        E element;
        Lock lock = new ReentrantLock();

        Node() {
            next = null;
            previous = null;
            element = null;
        }

        Node(Node previous, Node next, E element) {
            this.previous = previous;
            this.next = next;
            this.element = element;
        }

        Node(E element) {
            this(null, null, element);
        }

    }
}
