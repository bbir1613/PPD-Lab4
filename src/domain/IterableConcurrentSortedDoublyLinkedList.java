package domain;

import java.util.Iterator;

public class IterableConcurrentSortedDoublyLinkedList<E extends Comparable<E>> extends ConcurrentSortedDoublyLinkedList<E> implements Iterable<E> {

    @Override
    public Iterator<E> iterator() {
        return new ForwardReadIterator(first);
    }

    public Iterator<E> backwardIterator() {
        return new BackwardsReadIterator(last);
    }

    public Iterator<E> readWriteiterator() {
        return new ReadWriteIterator(first);
    }

    private class ForwardReadIterator implements Iterator<E> {
        private Node current;

        ForwardReadIterator(Node first) {
            this.current = first;
        }

        @Override
        public boolean hasNext() {
            return current.next.element != null;
        }

        @Override
        public E next() {
            current = current.next;
            return current.element;
        }
    }

    private class BackwardsReadIterator implements Iterator<E> {
        private Node current;

        BackwardsReadIterator(Node last) {
            this.current = last;
        }

        @Override
        public boolean hasNext() {
            return current.previous.element != null;
        }

        @Override
        public E next() {
            current = current.previous;
            return current.element;
        }
    }

    private class ReadWriteIterator implements Iterator<E> {
        private Node current;

        ReadWriteIterator(Node first) {
            this.current = first;
        }

        @Override
        public boolean hasNext() {
            return current.next.element != null;
        }

        @Override
        public E next() {
            current = current.next;
            return current.element;
        }

        @Override
        public void remove() {
            if (current.element == null) {
                next();
            }
            Node previous = current.previous;
            Node next = current.next;
            current.previous = null;
            current.next = null;
            previous.next = next;
            next.previous = previous;
            current = next;
        }
    }

}
